--
-- Built with,
--
--		  ,gggg,
--		 d8" "8I						 ,dPYb,
--		 88  ,dP						 IP'`Yb
--	  8888888P"							 I8  8I
--		 88								 I8  8'
--		 88		   gg	   gg	 ,g,	 I8 dPgg,
--	,aa,_88		   I8	   8I	,8'8,	 I8dP" "8I
-- dP" "88P		   I8,	  ,8I  ,8'	Yb	 I8P	I8
-- Yb,_,d88b,,_   ,d8b,  ,d8b,,8'_	 8) ,d8		I8,
--	"Y8P"  "Y888888P'"Y88P"`Y8P' "YY8P8P88P		`Y8
--

-- This is a starter colorscheme for use with Lush,
-- for usage guides, see :h lush or :LushRunTutorial

--		 Basically, name your file,
--		 "super_theme/lua/lush_theme/super_theme_dark.lua",
--
--		 not,
--		 "super_theme/lua/dark.lua".

-- Enable lush.ify on this file, run:
--	`:Lushify`

local lush = require('lush')
local hsl = lush.hsl

local black   = hsl"#000000"
local white   = hsl"#f0f6f0"
local gray	  = hsl"#727394"
local dark_gray = gray.darken(70)
local red	  = hsl"#f7005e"
local blue	  = hsl"#8855ee"
local green   = hsl"#00ea8c"
--local yellow	= hsl"#ffff00"
local cyan	  = hsl"#00ffff"
local desat_cyan = cyan.lighten(50).desaturate(25)
local magenta = hsl"#ff00ff"
local orange  = hsl"#ffa300"

-- LSP/Linters mistakenly show `undefined global` errors in the spec, they may
-- support an annotation like the following. Consult your server documentation.
---@diagnostic disable: undefined-global
local theme = lush( function (injected_functions)
	local sym = injected_functions.sym
	return {
	-- The following are the Neovim (as of 0.8.0-dev+100-g371dfb174) highlight
	-- groups, mostly used for styling UI elements.
	-- Comment them out and add your own properties to override the defaults.
	-- An empty definition `{}` will clear all styling, leaving elements looking
	-- like the 'Normal' group.
	-- To be able to link to a group, it must already be defined, so you may have
	-- to reorder items as you go.
	--
	-- See :h highlight-groups
	--
	-- ColorColumn	{ }, -- Columns set with 'colorcolumn'
	-- Conceal		 { }, -- Placeholder characters substituted for concealed text (see 'conceallevel')
	Cursor		 { fg = black, bg = white }, -- Character under the cursor
	lCursor		 { Cursor }, -- Character under the cursor when |language-mapping| is used (see 'guicursor')
	-- CursorIM		{ }, -- Like Cursor, but used when in IME mode |CursorIM|
	-- CursorColumn { }, -- Screen-column at the cursor, when 'cursorcolumn' is set.
	-- CursorLine	{ }, -- Screen-line at the cursor, when 'cursorline' is set. Low-priority if foreground (ctermfg OR guifg) is not set.
	-- Directory	{fg = red }, -- Directory names (and other special names in listings)
	DiffAdd		{ bg = green, fg = black, gui = "bold"}, -- Diff mode: Added line |diff.txt|
	DiffChange	{ bg = cyan, fg = black, gui = "bold"}, -- Diff mode: Changed line |diff.txt|
	DiffDelete	{ bg = red, fg = black, gui = "bold"}, -- Diff mode: Deleted line |diff.txt|
	DiffText		{ bg = blue, fg = black, gui = "bold"}, -- Diff mode: Changed text within a changed line |diff.txt|
	-- EndOfBuffer	{ }, -- Filler lines (~) after the end of the buffer. By default, this is highlighted like |hl-NonText|.
	-- TermCursor	{ }, -- Cursor in a focused terminal
	-- TermCursorNC { }, -- Cursor in an unfocused terminal
	ErrorMsg		{ bg = red, fg = black, gui = "bold"}, -- Error messages on the command line
	-- Folded		{ }, -- Line used for closed folds
	-- FoldColumn	{ }, -- 'foldcolumn'
	-- SignColumn	{ }, -- Column where |signs| are displayed
	-- IncSearch	{ }, -- 'incsearch' highlighting; also used for the text replaced with ":s///c"
	-- Substitute	{ }, -- |:substitute| replacement text highlighting
	LineNr		 { fg = gray }, -- Line number for ":number" and ":#" commands, and when 'number' or 'relativenumber' option is set.
	-- CursorLineNr { }, -- Like LineNr when 'cursorline' or 'relativenumber' is set for the cursor line.
	MatchParen	 { fg = black, bg = gray, gui = "bold" }, -- Character under the cursor or just before it, if it is a paired bracket, and its match. |pi_paren.txt|
	ModeMsg		 { fg = gray }, -- 'showmode' message (e.g., "-- INSERT -- ")
	MsgArea		{ fg = white }, -- Area for messages and cmdline
	-- MsgSeparator { }, -- Separator for scrolled messages, `msgsep` flag of 'display'
	-- MoreMsg		{ }, -- |more-prompt|
	NonText		 { fg = gray }, -- '@' at the end of the window, characters from 'showbreak' and other characters that do not really exist in the text (e.g., ">" displayed when a double-wide character doesn't fit at the end of the line). See also |hl-EndOfBuffer|.
	Normal		 { fg = white }, -- Normal text
	NormalFloat  { Normal, bg = dark_gray }, -- Normal text in floating windows.
	-- NormalNC		{ }, -- normal text in non-current windows
	Pmenu		 { fg = white, bg = dark_gray }, -- Popup menu: Normal item.
	PmenuSel	 { fg = blue, bg = dark_gray }, -- Popup menu: Selected item.
	PmenuSbar	 { bg = dark_gray }, -- Popup menu: Scrollbar.
	PmenuThumb	 { bg = gray }, -- Popup menu: Thumb of the scrollbar.
	-- Question		{ }, -- |hit-enter| prompt and yes/no questions
	-- QuickFixLine { }, -- Current |quickfix| item in the quickfix window. Combined with |hl-CursorLine| when the cursor is there.
	Search		 { bg = desat_cyan, fg = dark_gray }, -- Last search pattern highlighting (see 'hlsearch'). Also used for similar items that need to stand out.
	-- SpecialKey	{ }, -- Unprintable characters: text displayed differently from what it really is. But not 'listchars' whitespace. |hl-Whitespace|
	-- SpellBad		{ }, -- Word that is not recognized by the spellchecker. |spell| Combined with the highlighting used otherwise.
	-- SpellCap		{ }, -- Word that should start with a capital. |spell| Combined with the highlighting used otherwise.
	-- SpellLocal	{ }, -- Word that is recognized by the spellchecker as one that is used in another region. |spell| Combined with the highlighting used otherwise.
	-- SpellRare	{ }, -- Word that is recognized by the spellchecker as one that is hardly ever used. |spell| Combined with the highlighting used otherwise.
	--StatusLine   { }, -- Status line of current window
	--StatusLineNC { }, -- Status lines of not-current windows. Note: If this is equal to "StatusLine" Vim will use "^^^" in the status line of the current window.
	TabLine		 { }, -- Tab pages line, not active tab page label
	TabLineFill  { }, -- Tab pages line, where there are no labels
	-- TabLineSel	{ }, -- Tab pages line, active tab page label
	Title		 { fg = blue }, -- Titles for output from ":set all", ":autocmd" etc.
	Visual		 { fg = black, bg = gray }, -- Visual mode selection
	-- VisualNOS	{ }, -- Visual mode selection when vim is "Not Owning the Selection".
	WarningMsg	 { fg = red}, -- Warning messages
	Whitespace	 { fg = gray }, -- "nbsp", "space", "tab" and "trail" in 'listchars'
	Winseparator { fg = gray }, -- Separator between window splits. Inherts from |hl-VertSplit| by default, which it will replace eventually.
	-- VertSplit	 { Winseparator }, -- Column separating vertically split windows
	-- WildMenu		{ }, -- Current match in 'wildmenu' completion

	-- Common vim syntax groups used for all kinds of code and markup.
	-- Commented-out groups should chain up to their preferred (*) group
	-- by default.
	--
	-- See :h group-name
	--
	-- Uncomment and edit if you want more specific syntax highlighting.

	Comment		   { fg = gray }, -- Any comment

	Constant	   { fg = white }, -- (*) Any constant
	String		   { fg = green }, --	A string constant: "this is a string"
	Character	   { String }, --	A character constant: 'c', '\n'
	Number		   { fg = blue }, --   A number constant: 234, 0xff
	Boolean		   { Number }, --	A boolean constant: TRUE, false
	Float		   { Number }, --	A floating point constant: 2.3e10

	-- Identifier	  { fg = white }, -- (*) Any variable name
	Function	   { fg = cyan }, --   Function name (also: methods for classes)

	kw_ { fg = red },

	Statement	   { fg = red }, -- (*) Any statement
	Conditional    { kw_ }, --	 if, then, else, endif, switch, etc.
	Repeat		   { kw_ }, --	 for, do, while, etc.
	Label		   { fg = white }, --	 case, default, etc.
	Operator	   { fg = white }, --	"sizeof", "+", "*", etc.
	Keyword		   { kw_ }, --	 any other keyword
	Exception	   { fg = red, gui = "bold italic" }, --   try, catch, throw

	PreProc		   { fg = gray }, -- (*) Generic Preprocessor
	-- Include		  { fg = orange }, --	Preprocessor #include
	-- Define		  { fg = orange }, --	Preprocessor #define
	-- Macro		  { fg = orange }, --	Same as Define
	-- PreCondit	  { fg = orange }, --	Preprocessor #if, #else, #endif, etc.

	Type		   { fg = gray }, -- (*) int, long, char, etc.
	-- StorageClass   { }, --	static, register, volatile, etc.
	-- Structure	  { }, --	struct, union, enum, etc.
	-- Typedef		  { }, --	A typedef

	Special		  { fg = gray }, -- (*) Any special symbol
	SpecialChar    { fg = gray }, --   Special character in a constant
	-- Tag			  { bg = white }, --   You can use CTRL-] on this
	Delimiter	   { fg = gray }, --   Character that needs attention
	-- SpecialComment { }, --	Special things inside a comment (e.g. '\n')
	-- Debug		  { }, --	Debugging statements

	Underlined	  { fg = blue, gui = "underline" }, -- Text that stands out, HTML links
	Ignore		  { }, -- Left blank, hidden |hl-Ignore| (NOTE: May be invisible here in template)
	Error		  { bg = red, fg = black, gui = "bold italic"}, -- Any erroneous construct
	Todo			  { fg = gray, gui = "bold" }, -- Anything that needs extra attention; mostly the keywords TODO FIXME and XXX

	-- These groups are for the native LSP client and diagnostic system. Some
	-- other LSP clients may use these groups, or use their own. Consult your
	-- LSP client's documentation.

	-- See :h lsp-highlight, some groups may not be listed, submit a PR fix to lush-template!
	--
	-- LspReferenceText			   { } , -- Used for highlighting "text" references
	-- LspReferenceRead			   { } , -- Used for highlighting "read" references
	-- LspReferenceWrite		   { } , -- Used for highlighting "write" references
	-- LspCodeLens				   { } , -- Used to color the virtual text of the codelens. See |nvim_buf_set_extmark()|.
	-- LspCodeLensSeparator		   { } , -- Used to color the seperator between two or more code lens.
	-- LspSignatureActiveParameter { } , -- Used to highlight the active parameter in the signature help. See |vim.lsp.handlers.signature_help()|.

	-- See :h diagnostic-highlights, some groups may not be listed, submit a PR fix to lush-template!

	err_ { fg = red, gui = "bold" },
	wrn_ { fg = orange, gui = "bold" },
	inf_ { fg = white },

	DiagnosticError			  { err_ } , -- Used as the base highlight group. Other Diagnostic highlights link to this by default (except Underline)
	DiagnosticWarn			  { wrn_ } , -- Used as the base highlight group. Other Diagnostic highlights link to this by default (except Underline)
	DiagnosticInfo			  { inf_ } , -- Used as the base highlight group. Other Diagnostic highlights link to this by default (except Underline)
	DiagnosticHint			  { inf_ } , -- Used as the base highlight group. Other Diagnostic highlights link to this by default (except Underline)
	DiagnosticVirtualTextError { err_ } , -- Used for "Error" diagnostic virtual text.
	DiagnosticVirtualTextWarn  { wrn_ } , -- Used for "Warn" diagnostic virtual text.
	DiagnosticVirtualTextInfo  { inf_ } , -- Used for "Info" diagnostic virtual text.
	DiagnosticVirtualTextHint  { inf_ } , -- Used for "Hint" diagnostic virtual text.
	DiagnosticUnderlineError   { err_, gui = "underline" } , -- Used to underline "Error" diagnostics.
	DiagnosticUnderlineWarn	  { wrn_, gui = "underline" } , -- Used to underline "Warn" diagnostics.
	DiagnosticUnderlineInfo	  { inf_, gui = "underline" } , -- Used to underline "Info" diagnostics.
	DiagnosticUnderlineHint	  { inf_, gui = "underline" } , -- Used to underline "Hint" diagnostics.
	DiagnosticFloatingError	  { err_ } , -- Used to color "Error" diagnostic messages in diagnostics float. See |vim.diagnostic.open_float()|
	DiagnosticFloatingWarn	  { wrn_ } , -- Used to color "Warn" diagnostic messages in diagnostics float.
	DiagnosticFloatingInfo	  { inf_ } , -- Used to color "Info" diagnostic messages in diagnostics float.
	DiagnosticFloatingHint	  { inf_ } , -- Used to color "Hint" diagnostic messages in diagnostics float.
	DiagnosticSignError		  { err_ } , -- Used for "Error" signs in sign column.
	DiagnosticSignWarn		  { wrn_ } , -- Used for "Warn" signs in sign column.
	DiagnosticSignInfo		  { inf_ } , -- Used for "Info" signs in sign column.
	DiagnosticSignHint		  { inf_ } , -- Used for "Hint" signs in sign column.

	-- Tree-Sitter syntax groups.
	--
	-- See :h treesitter-highlight-groups, some groups may not be listed,
	-- submit a PR fix to lush-template!
	--
	-- Tree-Sitter groups are defined with an "@" symbol, which must be
	-- specially handled to be valid lua code, we do this via the special
	-- sym function. The following are all valid ways to call the sym function,
	-- for more details see https://www.lua.org/pil/5.html
	--
	-- sym("@text.literal")
	-- sym('@text.literal')
	-- sym"@text.literal"
	-- sym'@text.literal'
	--
	-- For more information see https://github.com/rktjmp/lush.nvim/issues/109

	-- sym"@text.literal"	   { }, -- Comment
	-- sym"@text.reference"    { }, -- Identifier
	-- sym"@text.title"		   { }, -- Title
	-- sym"@text.uri"		   { }, -- Underlined
	-- sym"@text.underline"    { }, -- Underlined
	-- sym"@text.todo"		   { }, -- Todo
	-- sym"@comment"		   { }, -- Comment
	-- sym"@punctuation"	   { }, -- Delimiter
	sym"@punctuation.bracket"		{ fg = white },
	sym"@punctuation.special"		{ fg = white },
	-- sym"@constant"		   { }, -- Constant
	sym"@constant.builtin"	{ fg = blue }, -- Special
	-- sym"@constant.macro"    { }, -- Define
	-- sym"@define"			   { }, -- Define
	-- sym"@macro"			   { }, -- Macro
	-- sym"@string"			   { }, -- String
	-- sym"@string.escape"	   { }, -- SpecialChar
	-- sym"@string.special"    { }, -- SpecialChar
	-- sym"@character"		   { }, -- Character
	-- sym"@character.special" { }, -- SpecialChar
	-- sym"@number"			   { }, -- Number
	-- sym"@boolean"		   { }, -- Boolean
	-- sym"@float"			   { }, -- Float
	sym"@function"			{ fg = cyan }, -- Function
	sym"@function.builtin"	{ fg = cyan }, -- Special
	sym"@function.call"		{ fg = cyan },
	sym"@function.macro"	{ fg = cyan }, -- Macro
	sym"@parameter"			{ fg = white }, -- Identifier
	sym"@method"			{ fg = cyan }, -- Function
	sym"@method.call"		{ fg = cyan }, -- Function
	sym"@field"				{ fg = white }, -- Identifier
	sym"@property"			{ fg = white }, -- Identifier
	sym"@constructor"		{ fg = blue }, -- Special
	-- sym"@conditional"	   { }, -- Conditional
	-- sym"@repeat"			   { }, -- Repeat
	-- sym"@label"			   { }, -- Label
	-- sym"@operator"		   { }, -- Operator
	sym"@keyword"			{ fg = red}, -- Keyword
	sym"@keyword.return"		   { fg = green}, -- Keyword
	-- sym"@exception"		   { }, -- Exception
	sym"@variable"			{ fg = white }, -- Identifier
	-- sym"@type"			   { }, -- Type
	-- sym"@type.definition"   { }, -- Typedef
	-- sym"@storageclass"	   { }, -- StorageClass
	-- sym"@structure"		   { }, -- Structure
	sym"@namespace"			{ fg = white }, -- Identifier
	-- sym"@include"		   { }, -- Include
	-- sym"@preproc"		   { }, -- PreProc
	-- sym"@debug"			   { }, -- Debug
	-- sym"@tag"			   { }, -- Tag

	WhichKey {fg = cyan},
	WhichKeyDesc {fg = white},
	WhichKeyGroup {fg = gray},

} end )

-- Return our parsed theme for extension or use elsewhere.
return theme
