
" \''''__              ____
"   )    `''''\_      --/ o"-__,
"   `_          (_    ./ ,/----´
"     \__         (_./  /
"        \__.          \___----^__
"         _´   _                  \
"  |    _´   .´ )\"\ _____         `\
"  |\__´   .´   ^ ^       `--._      )
"   `-__.-"                    \_____ )
"                    art by Ironwing "

let g:profile_mode=substitute(system('echo $profile_mode'),"\n","","")
" fancy | simple
function FancyProfile()
	return g:profile_mode == "fancy"
endfunction
function SimpleProfile()
	return g:profile_mode == "simple"
endfunction
" vim.g.profile_mode (lua)
" 	if profile_mode == "" then
" 		...
" 	end
function! GetNVimVersion()
    redir => s
    silent! version
    redir END
    return substitute(matchstr(s, 'NVIM v\zs[^\n]*'), "\\.", "", "g")
endfunction
let g:scope_ver = "090" " minimum telscope version

if empty(glob("~/.local/share/nvim/site/autoload/plug.vim"))
	silent! execute '!curl -fLo $HOME/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
	autocmd VimEnter * silent! PlugInstall
endif

" # pluggins

call plug#begin('~/.config/nvim/plugged')

" eregex - better regex with :%S
	"Plug 'othree/eregex.vim'

" lualine - status line
	Plug 'nvim-lualine/lualine.nvim'

" Zen Mode - distraction-free
	Plug 'folke/zen-mode.nvim'

" theme catpuccin
	"Plug 'catppuccin/vim', { 'as': 'catppuccin' }

" nvim-transparent
	Plug 'xiyaowong/nvim-transparent'

" Dashboard
	Plug 'glepnir/dashboard-nvim'

" Telescope (fzf-native requires clang/gcc)
	Plug 'nvim-lua/plenary.nvim'
	if (GetNVimVersion() > g:scope_ver)
	Plug 'nvim-telescope/telescope.nvim'
	Plug 'nvim-telescope/telescope-fzf-native.nvim', { 'do': 'make' }
	Plug 'nvim-telescope/telescope-file-browser.nvim'
	endif

" which-key.nvim
	Plug 'folke/which-key.nvim'

" Rainbow parenthesis
	Plug 'luochen1990/rainbow'

" Comment.nvim - comment lines
	Plug 'numToStr/Comment.nvim'

" mason - language manager
	Plug 'williamboman/mason.nvim'
	Plug 'williamboman/mason-lspconfig.nvim'

" nvim-treesitter (requires clang/gcc)
	Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
	Plug 'nvim-treesitter/nvim-treesitter-context'

" nvim-lspconfig
	Plug 'neovim/nvim-lspconfig'
	Plug 'weilbith/nvim-code-action-menu'

" null-ls
	Plug 'jose-elias-alvarez/null-ls.nvim'

" nvim-cmp - completion
	Plug 'hrsh7th/cmp-nvim-lsp'
	Plug 'hrsh7th/cmp-buffer'
	Plug 'hrsh7th/cmp-path'
	"Plug 'hrsh7th/cmp-cmdline'
	Plug 'hrsh7th/nvim-cmp'
	Plug 'hrsh7th/cmp-nvim-lua'
	Plug 'hrsh7th/cmp-omni', { 'for': ['jade', 'pug'] }

" Snippy - snippets (snipmate style)
	Plug 'dcampos/nvim-snippy'
	Plug 'dcampos/cmp-snippy'

" formatter.nvim
	Plug 'mhartington/formatter.nvim'

" colorizer - preview colors
	Plug 'lilydjwg/colorizer'

" indent-blankline - indentation
	Plug 'lukas-reineke/indent-blankline.nvim'

" indent-line - indentation
	"Plug 'Yggdroot/indentLine'

" indent-guide
	"Plug 'nathanaelkane/vim-indent-guides'

" vim-pug-complete
	Plug 'dNitro/vim-pug-complete', { 'for': ['jade', 'pug'] }

" vim-visual-multi
	Plug 'mg979/vim-visual-multi', {'branch': 'master'}

" -- language specific stuff

" odin
	Plug 'Tetralux/odin.vim'

" swift
	Plug 'keith/swift.vim'

" idris2
	Plug 'MunifTanjim/nui.nvim'
	Plug 'ShinKage/idris2-nvim'

" julia
	"Plug 'JuliaEditorSupport/julia-vim'

" nim
	"Plug 'alaviss/nim.nvim'
	"Plug 'zah/nim.vim'

" yue
	 Plug 'pigpigyyy/yuescript-vim'

" obsidian
	Plug 'epwalsh/obsidian.nvim'

" edgedb
	Plug 'edgedb/edgedb-vim'

" typst
	Plug 'kaarmu/typst.vim'

" endwise - do end, etc
"	Plug 'tpope/vim-endwise'

" unxtal
"	Plug 'karolbelina/uxntal.vim'

" nial
"	Plug 'niallang/Nial_Tooling', { 'dir': '~/vim'}

" vim-pug-complete
"	Plug 'dNitro/vim-pug-complete', { 'for': ['jade', 'pug'] }

" less
	"Plug 'groenewege/vim-less'

" Pkl
	Plug 'https://github.com/apple/pkl-neovim.git'

" Astro
	"Plug 'wuelnerdotexe/vim-astro'

" Lush
	Plug 'rktjmp/lush.nvim'
	Plug 'rktjmp/shipwright.nvim'

	Plug '~/.config/nvim/colors/koi-colors'
	"Plug '~/git/lush-template'

call plug#end()

" eregex
"let g:eregex_force_case = 1

" Rainbow Parenthesis
let g:rainbow_active = 0
let g:rainbow_conf = {
\	'guifgs':   ['#BCB8E6', '#8BC2CD', '#AF80E9', '#f6c177', '#67638D'],
\	'ctermfgs': ['lightgray','cyan', 'magenta', 'yellow', 'darkmagenta']	
\}

" ocp-index
set rtp+=/home/drag0n/.opam/default/share/ocp-index/vim

" # lua config
:lua << EOF
	local ok, mymod = pcall(require, 'indent_grid')
	if not ok then
		print(mymod)
	else
		
	end

	local profile_mode = vim.g.profile_mode
	local scope_ver = tonumber(vim.g.scope_ver)
	fancyProfile = function() return vim.call("FancyProfile") == 1 end
	simpleProfile = function() return vim.call("SimpleProfile") == 1 end
	getNVimVersion = function() return tonumber(vim.call("GetNVimVersion")) end
	local home = vim.loop.os_homedir()

	require "transparent".setup {
		--enable = true,
		extra_groups = {
			"StatusLine", "StatusLineNC",
			"TabLine", "TabLineFill", "TabLineSel", "ZenBg",
		},}

	require "zen-mode".setup {
		on_open = function(_) -- win
			print("") end,
		on_close = function()
			print("") end }

	if getNVimVersion() > scope_ver then
	do local telescope = require "telescope"
	telescope.setup {
		defaults = { sorting_strategy = 'ascending' },
		extensions = {
			fzf = { fuzzy = false },
			file_browser = { grouped=true },
		}}
	telescope.load_extension('fzf')
	telescope.load_extension('file_browser')
	end --
	end -- if getNVimVersion() > scope_ver then

	do local dash = require('dashboard')
	--dash.custom_header = {
	--	"\\''''__              ____               ",
	--	"  )    `''''\\_      --/ o\"-__,         ",
	--	"   `_          (_    ./ ,/----´          ",
	--	"    \\__         (_./  /                 ",
	--	"       \\__.          \\___----^__       ",
	--	"         _´   _                  \\      ",
	--	" |    _´   .´ )\\\"\\ _____         `\\  ",
	--	"  |\\__´   .´   ^ ^       `--._      )   ",
	--	"   `-__.-\"                    \\_____ ) ",
	--	"    art by Ironwing                 \"   ",
	--	"",
	--}
	--dash.custom_center = {
	--	{icon = "  ",
	--	desc = '𝙣ew                         ',
	--	action = 'DashboardNewFile'
	--	},
	--	{icon = "   ",
	--	desc = '𝙧ecent                       ',
	--	action = 'Telescope oldfiles',
	--	},
	--	{icon = "  ",
	--	desc = "𝙛iles                       ",
	--	--action = 'Explore',
	--	action = 'Telescope file_browser',
	--	},
	--	{icon = "  ",
	--	desc = "𝙩erminal                    ",
	--	action = 'terminal',
	--	},
	--	{icon = " <- ",
	--	desc = '𝙦uit',
	--	action = 'q',
	--	},}
	--dash.custom_footer = { io.popen('date +"%a, %d %b."'):read() }
	--dash.custom_footer = { "hello there!" }
	--dash.setup()
	end --

	require "which-key".setup {
		show_help = false,
		window = { padding = {1,1,1,1}, margin = {0,1,0,1} },
		layout = { height = { min = 4, max = 11 }, align = "center" },
		icons = { breadcrumb = "/", separator = "", group = "/" },
		plugins = {
			registers = false,
			spelling = { enabled = true, suggestions = 10 },
		},
	}

	-- visual: gc - line comment, gb - bloc
	-- insert: gcO - above, gco - below, gcA - end
	require "Comment".setup{
		padding = false,
		toggler = nil,
	}
	do local cft = require "Comment.ft"
	-- cft.odin = cft.lang('c')
	end --

	require "mason".setup()
	require "mason-lspconfig".setup {
		ensure_installed = (fancyProfile() and { "lua_ls" } or {})
	}

	require "nvim-treesitter.configs".setup {
		ensure_installed = { "vim", "lua" },
		highlight = {
			enable = true, additional_vim_regex_highlighting = false },
		indent = { enable = true }
	}
	require "treesitter-context".setup {
		enable = true,
		max_lines = 1, trim_scope = 'inner',
	}

	-- LSP servers - may require external setup
	do local servers = (fancyProfile() and {
		--[[
		'clangd', 'html', 'denols', 'cssls', 'sumneko_lua',
		--]]
		'ocamllsp', 'pylsp', 'julials', 'clangd', 'emmet_ls', 'astro',
		'ols', 'gopls', 'crystalline', 'slint_lsp'
	} or {})

	for _,s in ipairs(servers) do
		require "lspconfig"[s].setup {
			on_attach = on_attach,
			flags = lsp_flags,
		}
	end

	-- custom configs
	if fancyProfile() then

	require 'lspconfig'.lua_ls.setup { settings = { Lua = {
		runtime = { version = 'LuaJIT' },
		diagnostics = {
			-- Get the language server to recognize the `vim` global
			globals = {'vim'},
		},
		workspace = {
			-- Make the server aware of Neovim runtime files
			library = vim.api.nvim_get_runtime_file("", true),
		},
		telemetry = { enable = false },
	}, }, }

	require 'lspconfig'.denols.setup {
		cmd = { 'deno', 'lsp', '--unstable' },
		init_options = {
			enable = true,
			unstable = true,
		},
	}
	
	require 'lspconfig'.sourcekit.setup {
	}

	--[[
	require'lspconfig'.phpactor.setup {
		on_attach = on_attach,
		init_options = {
			-- $ composer require --dev psalm/phar
			["language_server_psalm.enabled"] = true,
		}
	}
	--]]

	do -- --
	local function has_tag(ln, tag)
		local lns = vim.api.nvim_buf_get_lines(0,ln-1,ln,false)[1]
		if lns == nil then return false end
		return lns:match(tag) ~= nil
	end
	local function fold_or(group)
		local res = false
		for _,v in pairs(group) do res = res or v end
		return res
	end
	local function format_diagnostics(params, client_id, filter_out)
		local tmp_d = {}
		for _, diagnostic in ipairs(params.diagnostics) do
			if not (filter_out ~= nil and filter_out(diagnostic)) then
				table.insert(tmp_d, diagnostic)
			end
		end
		params.diagnostics = tmp_d
		return vim.lsp.diagnostic.on_publish_diagnostics(nil, params, client_id)
	end
	local function filter_diagnostics(diagnostic)
		local ln = diagnostic.range.start.line
		-- https://github.com/microsoft/TypeScript/blob/main/src/compiler/diagnosticMessages.json
		local code = diagnostic.code
		--local severity = diagnostic.severity
		local filters = {}
		filters.common = code == 80001 -- filter commonjs suggestion
		filters.ignore = code == 6133 and ( -- ignore unused vars
			             has_tag(ln,"@ts%-ignore") or
			             has_tag(ln,"eslint%-disable%-next%-line no%-unused%-vars"))
		if fold_or(filters) then
			return true
		end
		return false
	end
	--local util = require 'lspconfig.util'
	--require'lspconfig'.tsserver.setup {
	--	root_dir = function(fname)
	--		return util.root_pattern 'tsconfig.json'(fname)
	--		or util.root_pattern('package.json', 'jsconfig.json')(fname)
	--	end,
	--	root_dir =
	--		[[root_pattern("package.json", "tsconfig.json", "jsconfig.json")]],
	--	handlers = {
	--		['textDocument/publishDiagnostics'] = function(_, params, client_id)
	--			return format_diagnostics(params, client_id, filter_diagnostics)
	--		end }}
	end -- --

	--[[
	do -- --
	local lspconfig = require'lspconfig'
	local configs = require'lspconfig.configs'
	local util = require 'lspconfig/util'
	if not configs.odin then
		configs.odin = { default_config = {
				cmd = {home.."/app/odin/ols/ols"},
				filetypes = {'odin'},
				root_dir = util.root_pattern('ols.json'),
				settings = {}
			}}
	end
	lspconfig.odin.setup{}
	end -- --
	--]]

	end --

	do local parser_config = require "nvim-treesitter.parsers".get_parser_configs()
	parser_config.swift = {
		install_info = {
			url = "/home/drag0n/git/tree-sitter-swift", -- local path or git repo
			--branch = "with-generated-files",
			files = {"src/parser.c", "src/scanner.c"}, -- some parsers also require src/scanner.c or src/scanner.cc
			-- optional entries:
			generate_requires_npm = false, -- if stand-alone parser without npm dependencies
			requires_generate_from_grammar = false, -- if folder contains pre-generated src/parser.c
		},
	}
	parser_config.alumina = {
		install_info = {
			url = "https://github.com/alumina-lang/tree-sitter-alumina.git",
			branch = "main",
			files = {"src/parser.c"},
			-- optional entries:
			generate_requires_npm = true,
			requires_generate_from_grammar = true,
		},
		filetype = "alumina",
	}
	end -- parser_config

	end -- if fancyProfile()

	vim.g.code_action_menu_window_border = 'rounded'

	if fancyProfile() then
	require "formatter".setup {
		filetype = {
			less = { function()
			local util = require "formatter.util"
			return {
				exe = "prettier",
				args = {
					"--use-tabs",
					"--stdin-filepath",
					util.escape_path(util.get_current_buffer_file_path()),
					"--parser",
					parser,
				},
				stdin = true,
				try_node_modules = true,
			} end },
			html = { function()
			local util = require "formatter.util"
			return {
				exe = "prettier",
				args = {
					"--use-tabs",
					"--stdin-filepath",
					util.escape_path(util.get_current_buffer_file_path()),
					"--parser",
					parser,
				},
				stdin = true,
				try_node_modules = true,
			} end },
			javascript = { function()
			local util = require "formatter.util"
			return {
				exe = "prettier",
				args = {
					"--use-tabs",
					"--stdin-filepath",
					util.escape_path(util.get_current_buffer_file_path()),
					"--parser",
					parser,
				},
				stdin = true,
				try_node_modules = true,
			} end },
		}
	}
	end -- if fancyProfile()
	-- cmp capabilities
	local capabilities = require('cmp_nvim_lsp')
		.default_capabilities(vim.lsp.protocol.make_client_capabilities())
	-- Setup nvim-cmp.
	do
	local cmp = require "cmp"
	local snippy = require "snippy"
	local has_words_before = function()
		local line, col = unpack(vim.api.nvim_win_get_cursor(0))
		return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
	end
	cmp.setup {
		snippet = {
			expand = function(args)
				require "snippy".expand_snippet(args.body)
			end,
		},
		mapping = {
			['<Tab>'] = cmp.mapping(function(fallback)
				if cmp.visible() then cmp.select_next_item()
				elseif snippy.can_expand_or_advance() then
					snippy.expand_or_advance()
				elseif has_words_before() then cmp.complete()
				else fallback() end
			end, { "i", "s" }),
			['<S-Tab>'] = cmp.mapping(function(fallback)
				if cmp.visible() then cmp.select_prev_item()
				elseif snippy.can_jump(-1) then snippy.previous()
				else fallback() end
			end, { "i", "s" }),
			['<C-u>'] = cmp.mapping(cmp.mapping.scroll_docs(-4), { 'i', 'c' }),
			['<C-d>'] = cmp.mapping(cmp.mapping.scroll_docs(4), { 'i', 'c' }),
			['<C-c>'] = cmp.mapping({
				i = cmp.mapping.abort(),
				c = cmp.mapping.close(),
			}),
			['<CR>'] = cmp.mapping.confirm { select = false },
			-- `select = false` only confirms explicitly selected items.
		},
		sources = {
			{ name = 'nvim_lsp' },
			{ name = 'snippy' },
			{ name = 'nvim_lua' },
			{ name = 'path' },
			{ name = 'buffer' },
		}
	}
	cmp.setup.filetype('pug', {
		sources = cmp.config.sources {
			{ name = 'omni'},
			{ name = 'snippy' },
			{ name = 'path'},
			{ name = 'buffer'},
		}
	})
	end --
	--local capabilities = require('cmp_nvim_lsp').update_capabilities(vim.lsp.protocol.make_client_capabilities())

	--require("idris2").setup({})

	do --
	local colors = {
		black  = '#191724',
		grey   = '#1D1D26',
		white  = '#f0f6f0',
		blue   = '#00ffff',
		cyan   = '#00ea8c',
		red    = '#f7005e',
		violet = '#8855ee',
	}
	local bubbles_theme = {
		normal = {
			a = { fg = colors.black, bg = colors.violet },
			b = { fg = colors.white, bg = colors.grey },
			c = { fg = colors.transparent, bg = colors.transparent },
		},

		insert = { a = { fg = colors.black, bg = colors.blue } },
		visual = { a = { fg = colors.black, bg = colors.cyan } },
		replace = { a = { fg = colors.black, bg = colors.red } },

		inactive = {
			a = { fg = colors.white, bg = colors.transparent },
			b = { fg = colors.white, bg = colors.transparent },
			c = { fg = colors.transparent, bg = colors.transparent },
		},
	}
	require('lualine').setup {
		options = {
			icons_enabled = false,
			theme = bubbles_theme,
			section_separators = { left = '', right = '' },
			component_separators = { left = '', right = '' },
			disabled_filetypes = {},
			always_divide_middle = false,
		},
		sections = {
			lualine_a = {
				{'mode', fmt = function (str) return string.lower(str) end,
					separator={left = ' ', right = ''}} },
			lualine_b = { 'filename' },
			lualine_c = {},
			lualine_x = {},
			lualine_y = { --[['filetype',]] 'progress' },
			lualine_z = {
				{'location', separator={left = '', right = ' '}} },
		},
		inactive_sections = {
			lualine_a = {
				{'filename', color={fg=colors.white,bg=colors.grey},
					separator={left = ' ', right = ''}} },
			lualine_b = {},
			lualine_c = {},
			lualine_x = {},
			lualine_y = {},
			lualine_z = { 'location' },
		},
		tabline = {}, extensions = {}
	}
	end

	vim.opt.list = true
	vim.opt.listchars= ("tab:   ,extends:~,precedes:<,nbsp:_,trail:·")
	--vim.opt.listchars= ("tab:│ ,extends:~,precedes:<,nbsp:_,trail:·")
	require('ibl').setup {
		exclude = {
			filetypes = {'pug', 'python', 'yaml', 'idris2', 'text', 'yue'}
		},
		indent = {
			char = {'│'}
		},
		scope = {
			enabled = false,
		},
		--char_list = {'│'}, -- (  ⃒ ⎸ │ )
		--char_list_blankline = {'┆'}, -- ( । ┆ )
		--show_first_indent_level = false,
		--show_trailing_blankline_indent = false,
		--strict_tabs = true,
		--show_current_context = true,
	}
	local hooks = require "ibl.hooks"
	hooks.register(hooks.type.WHITESPACE,
		hooks.builtin.hide_first_space_indent_level
	)
	hooks.register(hooks.type.WHITESPACE,
		hooks.builtin.hide_first_tab_indent_level
	)

	require "obsidian" .setup {
		dir = "~/doc/notes",
		completion = { nvim_cmp = true }
	}

EOF

let g:indentLine_fileType = ['idris2', 'python', 'yaml', 'pug',]
let g:indentLine_char = '·'
let g:indentLine_first_char = '·'
let g:indentLine_showFirstIndentLevel = 1

" # settings

try
	colorscheme koi-colors
catch
endtry

let mapleader = " " " space bar

set encoding=utf-8
set history=200
syntax enable
set termguicolors
set background=dark
highlight Comment cterm=italic

" tab stuff
set smarttab

"set noexpandtab
"set tabstop=4     " 1 tab = 4 spaces
""set softtabstop=0 " 4
"set shiftwidth=4
set expandtab
set tabstop=2     " 1 tab = 4 spaces
"set softtabstop=0 " 4
set shiftwidth=2

set autoindent    " Auto indent
set copyindent
set preserveindent

set nowrap      " don't wrap lines
set number      " numbers
set virtualedit=block
set mouse=a
" Cursor lines-when moving j/k
set so=7        " scrolloff
set wildmenu
set ignorecase
set smartcase
" Search results
set hlsearch
set showtabline=1
set nofoldenable

set bufhidden=unload " hide unload delete wipe
set splitbelow
set splitright

" when using wrap, keep words together,
" and indentation level
set lbr
set breakindent

" terminal lines to keep
set scrollback=1500

" ntrw file broser thing
let g:netrw_banner = 0
let g:netrw_liststyle = 3

" # remaps

nnoremap <leader>w :w<cr>
nnoremap <leader>q :q<cr>
nnoremap <leader>e :Telescope file_browser<cr>

" Buffer splitting
" split and select next split
nnoremap <leader>sv :vsplit \| wincmd p<cr>
nnoremap <leader>sh :split \| wincmd p<cr>

" Move trough wrapped lines
inoremap <silent> <Up> <C-o>gk
inoremap <silent> <Down> <C-o>gj
noremap <expr> j v:count ? 'j' : 'gj'
noremap <expr> k v:count ? 'k' : 'gk'

nnoremap <silent> <A-k> :m-2<CR>
nnoremap <silent> <A-j> :m+<CR>

" quick way to move between windows
nnoremap <C-h> <C-W>h
nnoremap <C-j> <C-W>j
nnoremap <C-k> <C-W>k
nnoremap <C-l> <C-W>l

inoremap <C-h> <Esc><C-W>hi
inoremap <C-j> <Esc><C-W>ji
inoremap <C-k> <Esc><C-W>ki
inoremap <C-l> <Esc><C-W>li

" managing tabs
nnoremap <leader>T :tabnew<cr>
nnoremap <leader>tq :tabclose<cr>
nnoremap <C-n> :tabnext<cr>
nnoremap <C-p> :tabprevious<cr>
" Opens a new tab with the current buffer's path
nnoremap <leader>te :tabedit <c-r>=expand("%:p:h")<cr>/

" spell checking
nnoremap <leader>ss :setlocal spell!<cr> " toggle
nnoremap <leader>sn ]s
nnoremap <leader>sp [s
nnoremap <leader>s? z=
set spelllang=en,pt
command! Spt set spelllang=pt
command! Sen set spelllang=en

" Some qol stuff
cabbrev W w
cabbrev Q q
cabbrev Wq wq
cabbrev WQ wq

" toggle zen mode
nnoremap <silent> <leader>z  :ZenMode<cr>

" terminal mode things
tnoremap <C-x><C-h> <C-\><C-n><C-W>h
tnoremap <C-x><C-j> <C-\><C-n><C-W>j
tnoremap <C-x><C-k> <C-\><C-n><C-W>k
tnoremap <C-x><C-l> <C-\><C-n><C-W>l
tnoremap <C-x>sv <C-\><C-n>:vnew \| terminal<cr>
tnoremap <C-x>sh <C-\><C-n>:new \| terminal<cr>
tnoremap <C-x>Sv <C-\><C-n>:vnew<cr>
tnoremap <C-x>Sh <C-\><C-n>:new<cr>
tnoremap <C-x>tt <C-\><C-n>:tabnew<cr>
tnoremap <C-x>tT <C-\><C-n>:tabnew \| terminal<cr>
" exit terminal mode
tnoremap <C-x>x <C-\><C-n>
tnoremap <C-x><Esc> <C-\><C-n>
"tnoremap <C-x><Esc> <Esc>

" lsp show info
nnoremap <silent> <leader>li :lua vim.lsp.buf.hover()<cr>
nnoremap <silent> <leader>ld :lua vim.diagnostic.open_float()<cr>
nnoremap <silent> <leader>la :CodeActionMenu<cr>
command! Issues Telescope diagnostics
command! Fixes  CodeActionMenu
"command! Format :lua vim.lsp.buf.formatting_sync()
command! Symbols :lua require'telescope.builtin'.lsp_document_symbols()
command! Rename :lua vim.lsp.buf.rename()

command! Files Telescope file_browser
command! Edit Telescope file_browser
command! Buffers Telescope buffers

command! HlGroups :so $VIMRUNTIME/syntax/hitest.vim

" Disable Arrow keys in normal mode
nnoremap <up> <nop>
nnoremap <down> <nop>
nnoremap <left> <nop>
nnoremap <right> <nop>
" not accidentally start recording macros and other stuff
nnoremap qq <nop>
nnoremap qQ <nop>
nnoremap q: <nop>
nnoremap Q <nop>
nnoremap <leader>c <nop>

" # custom functions

" Return to last edit position when opening files
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

if (FancyProfile())
augroup dashBoard
	autocmd!
	au FileType dashboard nnoremap <buffer> n :lua require'dashboard'.new_file(); print(' ')<cr>
	au FileType dashboard nnoremap <buffer> r :Telescope oldfiles<cr>
	au FileType dashboard nnoremap <buffer> f :Telescope file_browser<cr>
	au FileType dashboard nnoremap <buffer> t :lua require'dashboard'.new_file(); vim.cmd('term')<cr>
	au FileType dashboard nnoremap <buffer> q :q<cr>
augroup end
endif

" make terminal panel more clean
function Ter_set() abort
	setlocal nonumber
	startinsert
endfunction

function ForceTab() abort
	set smarttab
	set noexpandtab
endfunction
command! ForceTab call AsciiMode()

" qol stuff for ascii art I guess
let s:ascii_en = 0
function AsciiMode() abort
	let s:ascii_en = 1
	syntax off
	setlocal virtualedit=all
	setlocal nonumber
	setlocal nolist
	setlocal nosmarttab
	" it gives an error if there's nothing
	" to replace, don't mind
	autocmd BufWritePre * :%s/\s\+$//e
	autocmd BufWritePre * :%s/\t/    /g
endfunction
command! Ascii call AsciiMode()

function OpenVifm() abort
	rightbelow 32vsplit
	setlocal wfw
	terminal 
		\ vifm . +only +'set timefmt= '
		\ +'nnoremap ot :!nvr -cc tabnew \%f<cr>'
		\ +'nnoremap q  :q<cr>'
		\ +'nnoremap s  <nop>'
		\ +'set vicmd=nvr\ -cc\ \"wincmd\ p\"'
	" don't :tree in ~ or big/deep folders
	" requires nvr (install from pip)
	" 'zx' to (un)fold folders in tree view
	file vifm
	"startinsert
endfunction
command! Vifm call OpenVifm()

augroup termStuff
	autocmd!
	au BufEnter zsh startinsert
	au BufEnter vifm startinsert
	au TermOpen * call Ter_set()
	au TermClose * call feedkeys("i")
augroup end
