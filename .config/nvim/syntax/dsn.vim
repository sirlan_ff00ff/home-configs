" File: dsn.vim
" Author: Keith Smiley
" Description: Dragon's Simple Notation (language)

if exists("b:current_syntax")
  finish
endif

syn match dsdComment "::.*$"
syn match keyValue "\~>"
syn match dsdList "<>"

syn match dsdKey ".\+\ze\( \~>\)"
syn match dsdKey ".\+\ze\( <>\)"

syn match dsdValue "\(\~> \)\@<=.\+"
syn match dsdValue "^\(\(\~>\|<>\|::\)\@!.\)*$"


let b:current_syntax = "dsd"
hi def link dsdComment Comment
hi def link keyValue   Operator
hi def link dsdList    Keyword
hi def link dsdKey     Identifier
hi def link dsdValue   String
