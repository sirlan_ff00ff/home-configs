#!/usr/bin/env sh

if [ "$USER" != "root" ]; then
    echo " # this has to be ran as root"
    exit 1
fi

# get which distro
# currently only looking for 'arch' or 'fedora'
# and well, I think I give up on fedora for now
DISTRO=$(cat /etc/os-release | grep ID_LIKE | cut -d "=" -f 2)
if [[ $DISTRO = "" ]]; then
	DISTRO=$(cat /etc/os-release | grep ^ID | cut -d "=" -f 2)
fi
if [[ $DISTRO = "nobara" ]]; then
	DISTRO="fedora"
fi

echo
echo " dragon's script for (re)installing main programs"
echo " - installing for: $DISTRO"

echo
echo " - there's also a script for installing user programs"
echo

case $DISTRO in
arch)
	echo " (with all options)"
	echo " - this might take about 40 minutes"
	echo " - this might add about 8 GiB of stuff"
;;
fedora)
	echo " (with all options)"
	echo " - this might take about 1h and 10 minutes"
	echo " - this might add 8 GiB of stuff"
;;
esac

echo

echo " ? install all besides core?"
echo " <gui core, general programs, gaming and wine, teming, dev, talk, distrobox>"
printf " (y/N): "; read ans
case $ans in
	[yY]|yes)
		i_all="1"
		i_gui_core="1"
		i_programs="1"
		i_gaming="1"
		i_theme="1"
		i_dev="1"
		i_talk="1"
		i_distro="1"
	;;
esac

if ! [[ $i_all ]]; then
echo

printf " ? install gui core? [firefox, wezterm, ...] (y/N): "; read ans
case $ans in
	[yY]|yes) i_gui_core="1" ;;
	*) i_gui_core="" ;;
esac

printf " ? install general programs? [gimp, kdenlive, rnote, ...] (y/N): "; read ans
case $ans in
	[yY]|yes) i_programs="1" ;;
	*) i_programs="" ;;
esac

printf " ? install gaming and wine? [steam, wine, ...] (y/N): "; read ans
case $ans in
	[yY]|yes) i_gaming="1" ;;
	*) i_gaming="" ;;
esac

printf " ? install theming? [gradience, ...] (y/N): "; read ans
case $ans in
	[yY]|yes) i_theme="1" ;;
	*) i_theme="" ;;
esac

printf " ? install dev? [podman, clang, ...] (y/N): "; read ans
case $ans in
	[yY]|yes) i_dev="1" ;;
	*) i_dev="" ;;
esac

printf " ? install talk? [discord, telegram-desktop] (y/N): "; read ans
case $ans in
	[yY]|yes) i_talk="1" ;;
	*) i_talk="" ;;
esac

printf " ? install distrobox? (y/N): "; read ans
case $ans in
	[yY]|yes) i_distro="1" ;;
	*) i_distro="" ;;
esac

fi # (if ! [[ $i_all ]]; then)

echo
echo "                (       "
echo "                 )      "
echo "             _.-~(~-.   "
echo "            (@\\\`---´/.  "
echo "           (´  \`._.´  \`)"
echo "            \`-..___..-´ "
echo " now go relax and make a cup of tea"
echo "     this might take a while..."
echo

case $DISTRO in
arch)
	alias pacman_="pacman -S --needed --noconfirm"
	alias yay_="sudo --user=$SUDO_USER yay -S --needed --noconfirm"
;;
fedora)
	alias dnf_="dnf install --assumeyes"
;;
esac
alias flatpak_="flatpak --noninteractive install"

function pkgm_install() {
	case $DISTRO in
	arch)
		pacman_ $1
	;;
	fedora)
		dnf_ $1
	;;
	esac
}

if [[ $DISTRO = "arch" ]]; then
	echo " # enabling multilib, parallel downloads, color, and check space"
	sed -i 's/#\[multilib\]/[multilib]/' /etc/pacman.conf
	sed -i '/\[multilib\]/!b;n;cInclude = /etc/pacman.d/mirrorlist' /etc/pacman.conf
	sed -i 's/#Color/Color/' /etc/pacman.conf
	sed -i 's/#CheckSpace/CheckSpace/' /etc/pacman.conf
	sed -i 's/#ParallelDownloads = .*/ParallelDownloads = 5/' /etc/pacman.conf
	echo " # updating pacman repos and keyring"
	pacman -Syy --noconfirm
	pacman -Sy --noconfirm --needed archlinux-keyring
fi

if ! command -v flatpak &> /dev/null; then
	echo " # installing flatpak"
	case $DISTRO in
	arch)
		pacman_ flatpak
	;;
	fedora)
		dnf_ flatpak
	;;
	esac
fi

echo " # adding flathub"
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

if [[ $DISTRO = "arch" ]]; then
	if ! command -v yay &> /dev/null; then
		echo " # installing yay"
		pacman_ git base-devel
		sudo --user=$SUDO_USER git clone https://aur.archlinux.org/yay-bin.git
		cd yay-bin
		sudo --user=$SUDO_USER makepkg -si --noconfirm
		cd ..
		rm -rf yay-bin
	fi
fi

if [[ $DISTRO = "fedora" ]]; then
	echo " # dnf tweaks"
	sed -i 's/fastestmirror.*//' /etc/dnf/dnf.conf
	sed -i 's/max_parallel_downloads.*//' /etc/dnf/dnf.conf
	echo "max_parallel_downloads=5" >> /etc/dnf/dnf.conf
	echo "fastestmirror=True" >> /etc/dnf/dnf.conf
	echo " # enabling rpm fusion"
	dnf_ \
	https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm \
	https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
	#dnf group update core
	echo " # getting config manager"
	yum install --assumeyes dnf-plugins-core
fi

# 'must have' programs
function core() {
	# exist in all targets' default repos, with the same name
	local basic="gcc make git python lua lsd bat tldr mosh zsh neovim python-pynvim fzf vifm btop rclone unzip xclip fx"
	pkgm_install "$basic"

	case $DISTRO in
	arch)
		pacman_ yadm reflector fuse2
		yay_ fastfetch
	;;
	fedora)
		dnf config-manager --add-repo https://download.opensuse.org/repositories/home:TheLocehiliosan:yadm/Fedora_Rawhide/home:TheLocehiliosan:yadm.repo
		dnf_ yadm fastfetch fuse util-linux-user python3-pip
	;;
	esac
};

echo " # installing core "
core

# 'must have' gui programs
function gui_core() {
	local basic="firefox libwacom libportal"
	pkgm_install "$basic"

	flatpak_ flathub \
		com.github.tchx84.Flatseal \
		org.cubocore.CoreKeyboard

	case $DISTRO in
	arch)
		pacman_ wezterm xf86-input-wacom
	;;
	fedora)
		flatpak_ flathub org.wezfurlong.wezterm
		dnf_ xorg-x11-drv-wacom
	;;
	esac
}

if [[ $i_gui_core ]]; then
	echo " # installing gui core"
	gui_core
fi

# general programs, many media stuff
function programs() {
	local basic="gimp krita inkscape obs-studio kdenlive xournalpp"
	pkgm_install "$basic"

	flatpak_ flathub com.github.flxzt.rnote
}

if [[ $i_programs ]]; then
	echo " # instlling programs"
	programs
fi

# gaming, also wine I guess
function gaming_and_wine() {
	local basic="steam wine winetricks gamemode lib32-gamemode"
	pkgm_install "$basic"

	flatpak_ flathub \
		com.heroicgameslauncher.hgl \
		com.usebottles.bottles
}

if [[ $i_gaming ]]; then
	echo " # instlling gaming and wine"
	gaming_and_wine
fi

# theming and stuff
function theming() {
	#local basic=""
	#$pkgm_install $basic

	flatpak_ flathub \
		com.github.GradienceTeam.Gradience \
		org.gtk.Gtk3theme.adw-gtk3 \
		org.gtk.Gtk3theme.adw-gtk3-dark
	flatpak override --filesystem=xdg-config/gtk-4.0
	flatpak override --filesystem=xdg-config/gtk-3.0

	case $DISTRO in
	arch)
		#yay -S adw-gtk3
		#echo "   - getting adw-gtk3, putting in ~/.local/share/themes"
		#local latest_=$(curl -s https://api.github.com/repos/lassekongo83/adw-gtk3/releases/latest \
		#| grep "tag_name" \
		#| awk '{print substr($2, 2, length($2)-3)}')
		#local latest__=$(echo $latest_ | sed -e 's/\./-/g')
		#mkdir -p ~/.local/share/themes
		#wget -qO - "https://github.com/lassekongo83/adw-gtk3/releases/download/$latest_/adw-gtk3$latest__.tar.xz"\
		#| tar -xJ -C ~/.local/share/themes
	;;
	fedora)
		#dnf --assumeyes copr enable nickavem/adw-gtk3
		#dnf_ adw-gtk3
	;;
	esac
}

if [[ $i_theme ]]; then
	echo " # instlling theming"
	theming
fi

# dev stuff
function dev() {
	local basic="julia ruby podman cmake clang luarocks rlwrap"
	pkgm_install "$basic"
}

if [[ $i_dev ]]; then
	echo " # instlling dev"
	dev
fi

# chatting
function talk() {
	local basic="discord telegram-desktop"
	pkgm_install "$basic"
}

if [[ $i_talk ]]; then
	echo " # instlling talk"
	talk
fi

if [[ $i_distro ]]; then
	echo " # instlling distrobox"
	case $DISTRO in
	arch)
		yay_ distrobox
	;;
	fedora)
		dnf_ distrobox
	;;
	esac
fi

echo
echo " # all finished!"
echo " - you might want to reboot"
echo
