#!/bin/sh

sudo sed -i 's/ConditionUser=!root/#ConditionUser=!root/' /etc/systemd/user/sockets.target.wants/pipewire-pulse.socket
sudo sed -i 's/ConditionUser=!root/#ConditionUser=!root/' /usr/lib/systemd/user/pipewire-pulse.service
