#!/bin/sh

# mount an rclone drive, useful for putting in an auto start for the session

drive_name="drive-crypt"
dir="$HOME/drive"

# check if dir exists, if not, create it
if [ -d "$dir" ] 
then
    rclone --vfs-cache-mode writes mount $drive_name: $dir
else
    echo "directory $dir didn't exist, created."
    mkdir $dir
    rclone --vfs-cache-mode writes mount $drive_name: $dir
fi

# if it has an issue unmounting, use 'fusermount -u path/to/dir'
# to open online ui 'rclone rcd --rc-web-gui'
# rclone config in ~/.config/rclone/rclone.config
