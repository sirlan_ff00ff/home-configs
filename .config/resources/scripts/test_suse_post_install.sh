#!/bin/sh

if [ "$USER" != "root" ]; then
    echo " # this has to be ran as root"
    exit 1
fi

echo
echo " dragon's post-install script for openSUSE (tumbleweed)"

echo
echo " # updating repos"
zypper ref

echo
echo " # updating system"
zypper dup

echo
echo " # enable packman"
zypper addrepo -cfp 90 'https://ftp.gwdg.de/pub/linux/misc/packman/suse/openSUSE_Tumbleweed/' packman

if [[ $(lspci | grep "NVIDIA") ]]; then
echo
	echo " # enable nvidia"
	zypper addrepo --refresh https://download.nvidia.com/opensuse/tumbleweed NVIDIA
fi

zypper ref
zypper dist-upgrade --from packman --allow-vendor-change
zypper install --from packman ffmpeg gstreamer-plugins-{good,bad,ugly,libav} libavcodec-full vlc-codecs

if [[ $(lspci | grep "NVIDIA") ]]; then
echo
	zypper install dkms x11-video-nvidiaG06 nvidia-glG06 kernel-firmware-nvidia nvidia-computeG06 nvidia-gfx06-kmp-default
	mkinitrd
fi

echo
echo " # "
