#!/bin/sh

echo
echo " dragon's script for (re)installing main programs (user installs)"

echo
echo " - there's also a script for installing systemwide programs"
echo

echo " ? install all?"
echo " <theming, dev>"
printf " (y/N): "; read ans
case $ans in
	[yY]|yes)
		i_all="1"
		i_dev="1"
		i_theme="1"
	;;
esac

if ! [[ $i_all ]]; then
echo

printf " ? install theming? [adw-gtk3] (y/N): "; read ans
case $ans in
	[yY]|yes) i_theme="1" ;;
	*) i_theme="" ;;
esac

printf " ? install dev? [deno, node, ...] (y/N): "; read ans
case $ans in
	[yY]|yes) i_dev="1" ;;
	*) i_dev="" ;;
esac

fi

echo

# theming and stuff
function theming() {
	echo "   - getting adw-gtk3, putting in ~/.local/share/themes"
	local latest_=$(curl -s https://api.github.com/repos/lassekongo83/adw-gtk3/releases/latest \
	| grep "tag_name" \
	| awk '{print substr($2, 2, length($2)-3)}')
	local latest__=$(echo $latest_ | sed -e 's/\./-/g')
	mkdir -p ~/.local/share/themes
	wget -qO - "https://github.com/lassekongo83/adw-gtk3/releases/download/$latest_/adw-gtk3$latest__.tar.xz"\
	| tar -xJ -C ~/.local/share/themes
	sed -i 's/gtk-theme-name=.*/gtk-theme-name=adw-gtk3/' ~/.config/gtk-3.0/settings.ini
	sed -i 's/gtk-theme-name=.*/gtk-theme-name=adw-gtk3/' ~/.config/gtk-4.0/settings.ini
}

if [[ $i_theme ]]; then
	echo " # instlling theming"
	theming
fi

# dev stuff
function dev() {
	if ! command -v node &> /dev/null; then
		curl -fsSL https://get.pnpm.io/install.sh | sh -;
		echo "might want to remove some stuff from the .zshrc/.bashrc"
	fi
	if ! command -v deno &> /dev/null; then
		curl -fsSL https://deno.land/install.sh | sh
	fi
}

if [[ $i_dev ]]; then
	echo " # instlling dev"
	dev
fi

echo
echo " # all finished!"
echo " - you might want to log out and back in"
echo
