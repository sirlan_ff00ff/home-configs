# lines starting with '#' are comments
# ignore blank lines, just for organization
# '<> source', without it, assume default
# package manager
# <> >< sh to execute

# assuming Arch / 'pacman'

# needed
base-devel
wget
curl
git
clang
make

# cool
lua
julia

# fancy 'ls'
lsd

# fancy 'cat'
bat

# quick alternative to 'man'
tldr

# home config manager
yadm

# better than 'neofetch'
fastfetch <> aur

# good aur helper and interface to 'pacman'
yay <> aur

# sort arch mirrors
reflector

# smoother than raw 'ssh'
mosh

# better than 'bash'
zsh

# best editor
neovim

# fancier than 'htop'
btop

# coolest terminal
wezterm

# terminal file manager
vifm

# browser
firefox

# file syncing/drive mounts
rclone

# talk
discord
telegram-desktop

# image editing
gimp
gimp-plugin-gmic

# drawing
krita

# vector art
inkscape

# foss video editing
kdenlive

# screen recording
obs-studio

# gaming
steam
heroic-games-launcher-bin <> aur

wine
winetricks

# note taking
rnote <> flatpak
# for old notes
xournalpp

# office
libreoffice-fresh

# flatpak stuff
flatpak
libportal

# wacom
libwacom
xf86-input-wacom

# development
nodejs
deno <> >< curl -fsSL https://deno.land/install.sh | sh

# flatpaks
bottles <> flatpak
gradience <> flatpak
flatseal <> flatpak

# theming
adw-gtk3 <> aur
org.gtk.Gtk3theme.adw-gtk3 <> flatpak
org.gtk.Gtk3theme.adw-gtk3-dark <> flatpak
