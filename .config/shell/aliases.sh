
#alias fetch="clear; fastfetch"
alias fetch="clear; neofetch"

alias termbin="nc termbin.com 9999"

alias wttr="curl 'wttr.in'"
alias wttr-h="curl 'wttr.in/:help'"

# alumina stuff
alias alumina-boot='podman run -v $(pwd):/workspace ghcr.io/alumina-lang/alumina-boot:latest'

if command -v utop &> /dev/null && [ -f "$HOME/.utopinit" ]; then
	alias utop="utop -init ~/.utopinit"
fi

love_path="$HOME/app/love-11.4-x86_64.AppImage"
if [ -f "$love_path" ]; then
	alias love="$love_path"
fi

if command -v flatpak &> /dev/null; then
	alias frogatto="flatpak run com.frogatto.Frogatto --widescreen --width 1850 --height 1000 --scale"
fi

# dev
if command -v pnpm &> /dev/null; then
	alias pnm="pnpm"
	alias pnmx="pnpm dlx"
fi

if command -v idris2 &> /dev/null &&; then
	alias idr2="rlwrap idris2"
fi

if command -v csi &> /dev/null &&; then
	alias csi="rlwrap csi"
fi

#function nv() {
#	__NV_PRIME_RENDER_OFFLOAD=1 __GLX_VENDOR_LIBRARY_NAME=nvidia $@
#}

case $HOST in
woods) # lenovo legion 5i
	alias kill-ds4drv='kill -KILL $(cat /tmp/ds4drv.pid); rm /tmp/ds4drv.pid'
	alias start-ds4drv='ds4drv --udp'
;;
forest) # dell inspiron 14r
	alias disable-prime-sync='xrandr --output eDP-1-1 --set "PRIME Synchronization" 0;  xrandr --output HDMI-1-1 --set "PRIME Synchronization" 0'
	alias enable-prime-sync='xrandr --output eDP-1-1 --set "PRIME Synchronization" 1;  xrandr --output HDMI-1-1 --set "PRIME Synchronization" 1'
	alias stop-upower='sudo systemctl stop upower.service'
;;
esac

# preformance stuff, some may be unsfe
alias toggle-compositor='qdbus org.kde.kglobalaccel /component/kwin invokeShortcut "Suspend Compositing"'
alias split_lock_mitigate-0='sudo sysctl kernel.split_lock_mitigate=0'
alias split_lock_mitigate-1='sudo sysctl kernel.split_lock_mitigate=1'
alias vm_max_map-up='sudo sysctl -w vm.max_map_count=16777216'

# -- home mount stuff {
case $HOST in
woods)
	_home_from="forest"
	_from_ip="192.168.10.8"
	alias mount-forest="_mount-home"   # yes, this is hacky
	alias unmount-forest="_unmount-home"
;;
forest)
	_home_from="woods"
	_from_ip="192.168.10.8"
	alias mount-woods="_mount-home"
	alias unmount-woods="_unmount-home"
;;
esac
_home_mnt="$HOME/$_home_from"
function _mount-home() {
	! [ -d "$_home_mnt" ] && mkdir $_home_mnt
	sshfs drag0n@$_from_ip:/home/drag0n $_home_mnt
}
function _unmount-home() {
	if [ -d "$_home_mnt" ]; then
		[ $(ls -A "$_home_mnt" | wc -l) -gt 0 ] && fusermount -u $_home_mnt
		rmdir $_home_mnt
	fi
}
# -- } home mount stuff 

#function modrinth_source () {
#	firefox $(curl $1 | fx .source_url)
#}
