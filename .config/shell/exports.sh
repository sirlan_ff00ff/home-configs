
# helix
helix_runtime_path="$HOME/.config/helix/runtime/"
if [ -d "$helix_runtime_path" ]; then
	export HELIX_RUNTIME=$helix_runtime_path
fi

# odin
odin_path="$HOME/app/odin"
if [ -d "$odin_path" ]; then
	export PATH="$PATH:$odin_path"
	export ODIN_ROOT="$odin_path"
fi

# (php) composer
composer_path="$HOME/.config/composer/vendor/bin/"
if [ -d "$composer_path" ]; then
	export PATH="${PATH}:$composer_path"
fi

# lua(rocks)
luarocks_path="$HOME/.luarocks"
if [ -d "$luarocks_path" ]; then
	export PATH="${PATH}:$luarocks_path/bin"
	export LUA_PATH="/usr/share/lua/5.4/?.lua;/usr/share/lua/5.4/?/init.lua;/usr/lib/lua/5.4/?.lua;/usr/lib/lua/5.4/?/init.lua;./?.lua;./?/init.lua;$luarocks_path/share/lua/5.4/?.lua;$luarocks_path/share/lua/5.4/?/init.lua"
	export LUA_CPATH='/usr/lib/lua/5.4/?.so;/usr/lib/lua/5.4/loadall.so;./?.so;4luarocks_path/lib/lua/5.4/?.so'
fi

# deno
deno_path="$HOME/.deno"
if [ -d "$deno_path" ]; then
	export DENO_INSTALL="$deno_path"
	export PATH="$DENO_INSTALL/bin:$PATH"
fi

# pnpm
export PNPM_HOME="/home/drag0n/.local/share/pnpm"
case ":$PATH:" in
  *":$PNPM_HOME:"*) ;;
  *) export PATH="$PNPM_HOME:$PATH" ;;
esac

pnpm_path="$HOME/.local/share/pnpm"
if [ -d "$pnpm_path" ]; then
	export PNPM_HOME="/home/drag0n/.local/share/pnpm"
	export PATH="$PNPM_HOME:$PATH"
fi
# pnpm end

# bun completions
[ -s "/home/drag0n/.bun/_bun" ] && source "/home/drag0n/.bun/_bun"
# bun
export BUN_INSTALL="$HOME/.bun"
export PATH="$BUN_INSTALL/bin:$PATH"

# asdf version manager
. "$HOME/.asdf/asdf.sh"
# append completions to fpath
fpath=(${ASDF_DIR}/completions $fpath)
# initialise completions with ZSH's compinit
autoload -Uz compinit && compinit
# rebar3 build tool thing
export PATH=/home/drag0n/.cache/rebar3/bin:$PATH

# ruby gems
gems_path_1="$HOME/.gem/ruby/3.0.0/bin"
gems_path_2="$HOME/.local/share/gem/ruby/3.0.0/bin"
if [ -d "$gems_path_1" ]; then
	export PATH="$PATH:$gems_path_1"
fi
if [ -d "$gems_path_2" ]; then
	export PATH="$PATH:$gems_path_2"
fi

# pack (idris2)
idris_path="$HOME/.pack/bin"
if [ -d "$idris_path" ]; then
	export PATH="$PATH:$idris_path"
fi

# go
export GOPATH="$HOME/app/go"
if [ -d "$GOPATH" ]; then
	export PATH="${PATH}:$GOPATH/bin"
fi

# roc
roc_path="$HOME/app/roc"
if [ -d "$roc_path" ]; then
	export PATH="${PATH}:$roc_path"
fi

# swift (env)
#export SWIFTENV_ROOT="$HOME/.local/share/swiftenv"
#if [ -d "$SWIFTENV_ROOT" ]; then
#	export PATH="$SWIFTENV_ROOT/bin:$PATH"
#	eval "$(swiftenv init -)"
#fi

# nial
nial_path="$HOME/prg/nial/Nial_Development/BuildNial/build"
nial_root="$HOME/prg/nial/Nial_Development/Nialroot"
if [ -d "$nial_path" ]; then
	export PATH="${PATH}:$HOME/prg/nial/Nial_Development/BuildNial/build"
fi
if [ -d "$nial_root" ]; then
	export NIALROOT="$HOME/prg/nial/Nial_Development/Nialroot"
fi

# psn00b psx
export PATH="${PATH}:$HOME/dev/psn00b/bin"
export PSN00BSDK_LIBS="$HOME/dev/psn00b/lib/libpsn00b"

# config stuff
export PKG_CONFIG_PATH="/usr/lib/pkgconfig"

# ocaml / opam
[[ ! -r $HOME/.opam/opam-init/init.zsh ]] || source $HOME/.opam/opam-init/init.zsh  > /dev/null 2> /dev/null

# fx settings
export FX_THEME=2
export FX_LANG=js
export FX_SHOW_SIZE=true

# cargo
cargo_path="$HOME/.cargo"
if [ -d "$cargo_path" ]; then
	export PATH="$PATH:$cargo_path/bin"
fi

# VST stuff
#export VST_PATH="$HOME/app/vst/vst"
#export VST_PATH="$HOME/.vst"
export VST_PATH="$HOME/app/vst/vst:$HOME/.vst"
export VST3_PATH="$HOME/app/vst/vst3"
