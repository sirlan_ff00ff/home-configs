local wezterm = require 'wezterm'

local catppuccin = require("colors/catppuccin").setup {
	sync = false,
	flavour = "mocha",
}

return {
	enable_wayland = false,

	check_for_updates = false,
	warn_about_missing_glyphs = false,
	exit_behavior = "Close",

	font = wezterm.font_with_fallback {
		--{family="Iosevka Term", weight="Light"},
		{ family = "Iosevka Dragon Mod", weight = "Regular" },
	},

	font_size = 12,
	freetype_load_target = "HorizontalLcd",
	bold_brightens_ansi_colors = true,

	--color_scheme = "rose-pine",
	colors = catppuccin,
	--window_background_opacity = 0.80,
	window_background_opacity = 1,

	enable_tab_bar = false,
	--audible_bell = "Disabled",
	audible_bell = "SystemBeep",

	visual_bell = {
		fade_in_duration_ms = 550,
		target = "CursorColor",
	},

	disable_default_mouse_bindings = false,
	mouse_bindings = {
		{ event = { Up = { streak = 1, button = "Left" } },
			mods = "NONE",
			action = wezterm.action { CompleteSelection = "PrimarySelection" },
		},
		{
			event = { Up = { streak = 1, button = "Left" } },
			mods = "CTRL",
			action = "OpenLinkAtMouseCursor",
		},
		{
			event = { Down = { streak = 1, button = "Left" } },
			mods = "CTRL",
			action = "Nop",
		},
	},

	disable_default_key_bindings = true,
	keys = {
		{ key = "c", mods = "CTRL|SHIFT", action = { CopyTo = "Clipboard" } },
		{ key = "v", mods = "CTRL|SHIFT", action = { PasteFrom = "Clipboard" } },
		{ key = "x", mods = "CTRL|SHIFT", action = "ActivateCopyMode" },

		{ key = "=", mods = "CTRL", action = "IncreaseFontSize" },
		{ key = "-", mods = "CTRL", action = "DecreaseFontSize" },
		{ key = "0", mods = "CTRL", action = "ResetFontSize" },

		{ key = "r", mods = "CTRL|SHIFT", action = "ReloadConfiguration" },
	},
}
