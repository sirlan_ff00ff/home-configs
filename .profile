
if ! command -v wezterm &> /dev/null; then
	if command -v org.wezfurlong.wezterm &> /dev/null; then
		alias wezterm=org.wezfurlong.wezterm
	fi
fi

if [ -n "$GTK_MODULES" ]; then
	GTK_MODULES="$GTK_MODULES:unity-gtk-module"
else
  GTK_MODULES="unity-gtk-module"
fi

if [ -z "$UBUNTU_MENUPROXY" ]; then
  UBUNTU_MENUPROXY=1
fi

export GTK_USE_PORTAL=1
export XDG_DESKTOP_PORTAL=1

