
# \''''__              ____
#   )    `''''\_      --/ o"-__,
#   `_          (_    ./ ,/----´
#     \__         (_./  /
#        \__.          \___----^__
#         _´   _                  \
#  |    _´   .´ )\"\ _____         `\
#  |\__´   .´   ^ ^       `--._      )
#   `-__.-"                    \_____ )
#                    art by Ironwing "

# requires:
# 	curl
# 	git
# 	neovim-remote :: pip

export profile_mode="fancy"
# fancy -> all the bells and whistles
# simple -> only a few terminal stuff
fecth_program="fastfetch"

# install and use zplug for zsh plugins
if [ ! -f "$HOME/.zplug/init.zsh" ]; then
	curl -sL --proto-redir -all,https \
	https://raw.githubusercontent.com/zplug/installer/master/installer.zsh \
	| zsh
fi
if [ -f "$HOME/.zplug/init.zsh" ]; then
	source ~/.zplug/init.zsh
	# declare plugins to use
	zplug "zsh-users/zsh-syntax-highlighting", defer:2
	zplug "zsh-users/zsh-history-substring-search"
	zplug "jeffreytse/zsh-vi-mode", defer:1
	# install plugins if there are plugins that have not been installed
	if ! zplug check --verbose; then
		printf "Install? [y/N]: "
		if read -q; then
			echo; zplug install
		fi
	fi
	zplug load
fi

# > go to vi section
#bindkey '^[[A' history-substring-search-up
#bindkey '^[[B' history-substring-search-down
#bindkey "$terminfo[kcuu1]" history-substring-search-up
#bindkey "$terminfo[kcud1]" history-substring-search-down
export HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_FOUND=""
export HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_NOT_FOUND=""
export HISTORY_SUBSTRING_SEARCH_PREFIXED=1

if [ -f "$HOME/.config/shell/git-prompt.sh" ]; then
	source $HOME/.config/shell/git-prompt.sh
	export GIT_PS1_SHOWUPSTREAM="auto"
	export GIT_PS1_SHOWDIRTYSTATE=1
	export GIT_PS1_SHOWCOLORHINTS=1
else
	__git_ps1=""
fi

setopt histignorealldups sharehistory prompt_subst

HISTSIZE=700
SAVEHIST=700
HISTFILE=~/.zsh_history

fpath+=~/.zfunc
# use modern completion system
autoload -Uz compinit
compinit

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' menu select=2
eval "$(dircolors -b)"
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true

# -- PS1

_arrow="→"
case "$(tty)" in
	"/dev/pts"*)
	_pat_at="at"
	_icn_ok="🌺"
	_icn_er="🥀"
	#_icn_ok="🌿" # alternate
	#_icn_er="🍂" #
	;;
	"/dev/tty"*)
	_pat_at="@"
	_icn_ok="♣"
	_icn_er="♦"

	/usr/bin/env clear
	echo "\n" $TTY '·' $(date "+ %H:%M · %a %e %b %Y") '· welcome' $USERNAME
	;;
	*) ;;
esac
function update_ps1() {
	local ps1path=" $_pat_at "'%F{8}%(5~|.../%4~|%~)'
	local gitstatus='$(__git_ps1 "· %s")'
	local SYMBOL='%B%(?,%F{green}'"$_icn_ok"',%F{red}'"$_icn_er"')%f%b'

	export PS1=$'\n'"$ps1path $gitstatus"$'\n'" $SYMBOL"' %F{blue}%B'"$_arrow"'%f%b '

  if [[ "${TERM_PROGRAM}" == "WarpTerminal" ]]; then
    export PS1=""
  fi

}; update_ps1

# -- vi mode

function vi-binds() {
	bindkey '^[[A' history-substring-search-up
	bindkey '^[[B' history-substring-search-down
}
zvm_after_init_commands+=(vi-binds)

ZVM_VI_EDITOR=$EDITOR
if [[ -n $VIMRUNTIME ]]; then
	# $ pip install neovim-remote
	ZVM_VI_EDITOR='nvr -cc split +"setlocal bufhidden=wipe" --remote-wait-silent'
fi
ZVM_VI_ESCAPE_BINDKEY='Xx'
ZVM_VI_VISUAL_ESCAPE_BINDKEY='Vv'
ZVM_VI_INSERT_ESCAPE_BINDKEY=$ZVM_VI_ESCAPE_BINDKEY
ZVM_VI_OPPEND_ESCAPE_BINDKEY=$ZVM_VI_ESCAPE_BINDKEY

function zvm_after_select_vi_mode() {
	case $ZVM_MODE in
	$ZVM_MODE_NORMAL)
		_arrow=":"
		update_ps1
	;;
	$ZVM_MODE_INSERT)
		_arrow="→"
		update_ps1
	;;
	$ZVM_MODE_VISUAL)
		_arrow="|"
		update_ps1
	;;
	$ZVM_MODE_VISUAL_LINE)
		_arrow=">"
		update_ps1
	;;
	$ZVM_MODE_REPLACE)
		_arrow="_"
		update_ps1
	;;
	esac
}

# -- (neo)vim stuff

if command -v nvim &> /dev/null; then
	export EDITOR='nvim'
	if [[ -n $VIMRUNTIME ]] && command -v nvr &> /dev/null; then
		# $ pip install neovim-remote
		export EDITOR="nvr"
		alias vsplit="nvr -cc split"
	fi
	export VISUAL=$EDITOR
	alias v=$VISUAL
fi

# -- aliases

alias cp="cp -i"

if command -v wezterm &> /dev/null; then
	alias newterm="wezterm cli spawn --cwd . --new-window -- $SHELL"
fi

if command -v lsd &> /dev/null; then
	alias ls="lsd --group-dirs first"
	alias la="lsd -la -F"
	function ls-tree() {
		if [ $PWD = $HOME ]; then
			lsd --tree --depth 2
		else
			lsd --tree
		fi
	}
	alias tree="ls-tree"
else
	alias ls="ls --color=auto --file-type --group-directories-first"
	alias la="ls --color=auto -la"
fi
alias l="\\ls"

alias :q=exit; alias q=exit; alias Q=exit; alias :Q=exit;
alias :wq=exit; alias wq=exit; alias Wq=exit; alias :Wq=exit;

#alias reload='source ~/.zshrc; reset'
alias reload='reset; source ~/.zshrc'

# -- -- stuff

fxB="\e[1m"      # bold
fxI="\e[3m"      # italic
fxU="\e[0m\e[4m" # underline
fxS="\e[0m\e[9m" # striketrough
cfx="\e[0m"      # clear format
# true colour
tcl="\e[38;2" # then ;R;G;Bm, like $tcl;100;100;23m
# 256 colours
cFF="\e[38;5" # then ;Cm, like $cFF;34m

# homebrew?
eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"

# -- -- custom aliases

if [ -f "$HOME/.config/shell/aliases.sh" ]; then
	source $HOME/.config/shell/aliases.sh
fi

# -- exports

unset SSH_ASKPASS
export PATH="$PATH:$HOME/.local/bin"

if [ -f "$HOME/.config/shell/exports.sh" ]; then
	source $HOME/.config/shell/exports.sh
fi

